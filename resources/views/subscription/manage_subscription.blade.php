<div class="row mt-5">
    <div class="col-md-12 text-center">
        <h5>
            Do you want to cancel your subscription?
            <a class="btn btn-danger" id="cancel_subscription_btn"
               onclick="cancelSubscription();">Cancel Now</a>
        </h5>
    </div>
</div>
