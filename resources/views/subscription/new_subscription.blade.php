<div class="row mt-3">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div id="show_payment_method" class="hidden">
            <div id="dropin-container"></div>
            <button id="submit-button" class="btn btn-primary">Pay Now</button>
        </div>
    </div>
    <div class="col-md-3"></div>
</div>
