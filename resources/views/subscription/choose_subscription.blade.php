<div class="row">
    <div class="col-md-12 text-center">
        <h5>
            Get deep insights by subscribing one of our plans
        </h5>
    </div>
</div>

<div class="row mt-4">
    @foreach($plans as $plan)
        <div class="col-sm-1"></div>
        <div class="col-sm-4 border border-primary text-center p-3 mb-2">
            <span><h2>{{ $plan->plan_name }}</h2></span>
            <span><h3 class="text-primary">${{ $plan->plan_price ." ". $plan->currency}}</h3></span>
            <span><small>{{ $plan->plan_description }}</small></span><br/>
            <button type="button" class="btn btn-info mt-3" onclick="showPaymentMethods('{{ $plan->id }}');">Subscribe</button>
        </div>
        <div class="col-sm-1"></div>
    @endforeach
</div>
<input type="hidden" id="plan_id">
@include('subscription.new_subscription')
