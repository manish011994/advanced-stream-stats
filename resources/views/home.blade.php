@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Twitch Stream Analytics</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="row">
                            <span class="col-md-2 border border-1 border-warning text-center p-2">
                                <span class="border-info border-bottom"> LIVE VIEWERS </span> </br>
                                2,699,226
                            </span>
                            <div class="col-md-1"></div>
                            <div class="col-md-2 border border-1 border-warning text-center p-2">
                                <span class="border-info border-bottom"> 7-DAY AVG. VIEWERS </span> </br>
                                2,535,094
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-2 border border-1 border-warning text-center p-2">
                                <span class="border-info border-bottom"> LIVE CHANNELS NOW </span> </br>
                                87,585
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-2 border border-1 border-warning text-center p-2">
                                <span class="border-info border-bottom"> 7-DAY AVG. CHANNELS </span> </br>
                                91,098
                            </div>
                        </div>
                        <div class="mb-3"></div>
                        @if($isSubscribedUser)
                            <div class="row">
                            <span class="col-md-2 border border-1 border-warning text-center p-2">
                                <span class="border-info border-bottom"> 24 HOURS AVG. VIEWERS</span> </br>
                                2,488,876
                            </span>
                                <div class="col-md-1"></div>
                                <div class="col-md-2 border border-1 border-warning text-center p-2">
                                    <span class="border-info border-bottom"> ALL-TIME PEAK VIEWERS </span> </br>
                                    6,647,412
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-2 border border-1 border-warning text-center p-2">
                                    <span class="border-info border-bottom"> 24 HOURS AVG. CHANNELS </span> </br>
                                    87,585
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-2 border border-1 border-warning text-center p-2">
                                    <span class="border-info border-bottom"> ALL-TIME PEAK CHANNELS </span> </br>
                                    233,935
                                </div>
                            </div>
                            <div class="mb-3"></div>
                            <div class="row">
                            <span class="col-md-2 border border-1 border-warning text-center p-2">
                                <span class="border-info border-bottom"> ACTIVE STREAMERS DAILY</span> </br>
                                1,159,812
                            </span>
                                <div class="col-md-1"></div>
                                <div class="col-md-2 border border-1 border-warning text-center p-2">
                                    <span class="border-info border-bottom"> LARGEST NUMBER OF STREAMERS </span> </br>
                                    9,894,745
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-2 border border-1 border-warning text-center p-2">
                                    <span class="border-info border-bottom"> HOURS WATCHED DAILY</span> </br>
                                    60,351,833
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-2 border border-1 border-warning text-center p-2">
                                    <span class="border-info border-bottom"> MAX AMOUNT OF HOURS WATCHED</span> </br>
                                    2,307,473,592
                                </div>
                            </div>
                            @include('subscription.manage_subscription')
                        @else
                            @include('subscription.choose_subscription')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script_content')
    <script>
        function showPaymentMethods(planId) {
            $("#plan_id").val(planId);
            $("#show_payment_method").show();
            var button = document.querySelector('#submit-button');

            braintree.dropin.create({
                authorization: '{{ env('BRAINTREE_AUTHORIZATION_KEY') }}',
                container: '#dropin-container',
                paypal: {
                    flow: 'vault'
                },
                vaultManager: true,
            }, function (createErr, instance) {
                if (createErr) {
                    alert(createErr);
                    return false;
                }
                button.addEventListener('click', function () {
                    instance.requestPaymentMethod(function (requestPaymentMethodErr, payload) {
                        if (requestPaymentMethodErr) {
                            alert(requestPaymentMethodErr);
                            return false;
                        }
                        console.log("payload", payload);
                        let formData = new FormData();
                        formData.append('payload', JSON.stringify(payload));
                        formData.append('plan_id', $("#plan_id").val());
                        // Submit payload.nonce to your server
                        $.ajax({
                            url: '{{ route('create_subscription') }}',
                            method: 'post',
                            dataType: 'json',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            beforeSend: function () {
                                $("#submit-button").html("Please wait... Payment processing...").attr('disabled', true);
                            },
                            success: function (result) {
                                if (result.success) {
                                    window.location.reload();
                                } else {
                                    alert(result.message);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                alert("Something went wrong! Please try again later.");
                            },
                            complete: function () {
                                $("#submit-button").html("Pay Now").removeAttr('disabled');
                            }
                        });
                    });
                });
            });
        }

        function cancelSubscription() {
            if (confirm('Are you sure to cancel the subscription? This action cannot be undone.')) {
                $.ajax({
                    url: '{{ route('cancel_subscription') }}',
                    method: 'post',
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $("#cancel_subscription_btn").html("Please wait... Cancellation processing...").attr('disabled', true);
                    },
                    success: function (result) {
                        if (result.success) {
                            window.location.reload();
                        } else {
                            alert(result.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Something went wrong! Please try again later.");
                    },
                    complete: function () {
                        $("#cancel_subscription_btn").html("Cancel Now").removeAttr('disabled');
                    }
                });
            }
        }
    </script>
@endsection
