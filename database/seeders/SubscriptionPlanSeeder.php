<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriptionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            [
                'plan_name' => 'Monthly Analytics Plan',
                'pg_plan_id' => 'twitch_monthly_subs',
                'plan_duration_value' => 1,
                'plan_duration_type' => 'month',
                'currency' => 'USD',
                'plan_price' => 5.99,
                'plan_description' => 'Witness the deep insights of the Twitch Analytics for a month. "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
                'status' => 'active',
                'created_at' => DB::raw('NOW()')
            ],
            [
                'plan_name' => 'Yearly Analytics Plan',
                'pg_plan_id' => 'twitch_yearly_subs',
                'plan_duration_value' => 1,
                'plan_duration_type' => 'year',
                'currency' => 'USD',
                'plan_price' => 59.99,
                'plan_description' => 'Witness the deep insights of the Twitch Analytics for an year. "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
                'status' => 'active',
                'created_at' => DB::raw('NOW()')
            ],
        ];
        DB::table('subscription_plans')->insert($plans);
    }
}
