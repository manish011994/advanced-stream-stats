<?php

namespace App\Http\Controllers;

use App\Models\SubscriptionPlans;
use App\Models\UserSubscriptionDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //Get User Details for its subscription status
        $user = Auth::user();
        $isSubscribedUser = false;
        if ($user->is_subscribed == 'yes') {
            $isSubscribedUser = true;
        }
        $plans = SubscriptionPlans::getActivePlans();
        $activePlan = UserSubscriptionDetails::checkActiveSubscription($user->getAuthIdentifier());
        return view('home', [
            'isSubscribedUser' => $isSubscribedUser,
            'plans' => $plans
        ]);

    }
}
