<?php

namespace App\Http\Controllers;

use App\Models\PaymentLogs;
use App\Models\SubscriptionPlans;
use App\Models\User;
use App\Models\UserSubscriptionDetails;
use Braintree\Gateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    protected $gateway;
    protected $userDetails;

    public function __construct()
    {
        $this->middleware('auth');
        $this->gateway = new Gateway([
            'environment' => env('BRAINTREE_ENVIRONMENT'),
            'merchantId' => env('BRAINTREE_MERCHANT_ID'),
            'publicKey' => env('BRAINTREE_PUBLIC_KEY'),
            'privateKey' => env('BRAINTREE_PRIVATE_KEY')
        ]);
    }

    public function createSubscription(Request $request)
    {
        $payload = $request->post('payload') ?? "";
        $payload = !empty($payload) ? @json_decode($payload) : "";
        PaymentLogs::savePaymentInitiateLog('nonce', $payload);
        $planId = $request->post('plan_id') ?? "";
        if (!empty($payload)) {
            $this->userDetails = Auth::user();
            $userId = $this->userDetails->getAuthIdentifier();

            $planDetails = SubscriptionPlans::find($planId);
            $planId = $planDetails->id;
            $pgPlanId = $planDetails->pg_plan_id;
            //Check if Customer is already registered on Braintree
            if (empty($this->userDetails->braintree_customer_id)) {

                //Create Customer on Braintree
                $customerCreate = $this->createCustomerWithPaymentMethod($payload);
                if ($customerCreate->success) {
                    //Update User with Braintree Customer Id
                    $user = User::find($userId);
                    $user->braintree_customer_id = "customer_" . $userId;
                    $user->save();
                } else {
                    return response()->json(['error' => true, 'message' => 'Failed in saving customer on Braintree. '
                        . $customerCreate->message]);
                }

                // Subscribe to the plan
                $paymentMethodToken = $customerCreate->customer->paymentMethods[0]->token;
                $subscribeDetails = $this->subscribePlan($paymentMethodToken, $pgPlanId);
            } else {
                // Customer already created on Braintree, create payment method and subscribe
                $paymentMethodToken = $this->createPaymentMethod($payload);
                if ($paymentMethodToken->success) {
                    $paymentMethodToken = $paymentMethodToken->paymentMethod->token;
                    $subscribeDetails = $this->subscribePlan($paymentMethodToken, $pgPlanId);
                } else {
                    return response()->json(['error' => true, 'message' => 'Failed in creating Payment Method. ' .
                        $paymentMethodToken->message]);
                }
            }

            if ($subscribeDetails->success) {
                try {
                    DB::beginTransaction();
                    // Subscription Successful, update entries and create entry in users subscription
                    $user = User::find($userId);
                    $user->is_subscribed = 'yes';
                    $user->subscribed_till = ($subscribeDetails->subscription->billingPeriodEndDate)->format('Y-m-d');
                    $user->save();

                    //Save Subscription Details
                    $saveSubscriptionDetails = UserSubscriptionDetails::saveUserSubscriptionDetails($userId, $planId, $subscribeDetails);
                    if ($saveSubscriptionDetails->id) {
                        DB::commit();
                        $request->session()->flash('status', 'Subscription successful!');
                        return response()->json(['success' => true]);
                    } else {
                        return response()->json(['error' => true, 'message' => 'Failed in saving details of User subscription.']);
                    }
                } catch (\Exception $exception) {
                    DB::rollBack();
                    return response()->json(['error' => true, 'message' => $exception->getMessage()]);
                }
            } else {
                return response()->json(['error' => true, 'message' => 'Subscription creation Failed.' .
                    $subscribeDetails->message]);
            }
        } else {
            return response()->json(['error' => true, 'message' => 'Invalid Request.']);
        }
    }

    public function createPaymentMethod($payload)
    {
        $paymentMethod = $this->gateway->paymentMethod()->create([
            'customerId' => $this->userDetails->braintree_customer_id,
            'paymentMethodNonce' => $payload->nonce,
        ]);
        PaymentLogs::savePaymentInitiateLog('payment_method', $paymentMethod);
        return $paymentMethod;
    }

    public function createCustomerWithPaymentMethod($payload)
    {
        $customerCreate = $this->gateway->customer()->create([
            'id' => 'customer_' . $this->userDetails->getAuthIdentifier(),
            'firstName' => $this->userDetails->name,
            'paymentMethodNonce' => $payload->nonce,
            'creditCard' => [
                'options' => [
                    'verifyCard' => true
                ]
            ]
        ]);
        PaymentLogs::savePaymentInitiateLog('create_customer', $customerCreate);
        return $customerCreate;
    }

    public function subscribePlan($paymentMethodToken, $pgPlanId)
    {
        $subscribe = $this->gateway->subscription()->create([
            'paymentMethodToken' => $paymentMethodToken,
            'planId' => $pgPlanId
        ]);
        PaymentLogs::savePaymentInitiateLog('subscribe', $subscribe);
        return $subscribe;
    }

    public function cancelSubscription(Request $request)
    {
        $userId = Auth::user()->getAuthIdentifier();
        $getActivePlan = UserSubscriptionDetails::checkActiveSubscription($userId);
        $subscriptionId = $getActivePlan->subscription_id;
        $cancelSubscription = $this->cancelPlan($subscriptionId);
        if ($cancelSubscription->success) {
            try {
                DB::beginTransaction();
                // Subscription Successful, update entries and create entry in users subscription
                $user = User::find($userId);
                $user->is_subscribed = 'no';
                $user->subscribed_till = NULL;
                $user->save();

                //Inactive User's Subscription Details
                UserSubscriptionDetails::inactivePreviousSubscriptions($userId);
                DB::commit();
                $request->session()->flash('status', 'Successfully unsubscribed from the Plan!');
                return response()->json(['success' => true]);
            } catch (\Exception $exception) {
                DB::rollBack();
                return response()->json(['error' => true, 'message' => $exception->getMessage()]);
            }
        } else {
            return response()->json(['error' => true, 'message' => 'Failed in cancelling Subscription. '
                . $cancelSubscription->message]);
        }
    }

    public function cancelPlan($subscriptionId)
    {
        $cancelSubscription = $this->gateway->subscription()->cancel($subscriptionId);
        PaymentLogs::savePaymentInitiateLog('cancel_subscription', $cancelSubscription);
        return $cancelSubscription;
    }
}
