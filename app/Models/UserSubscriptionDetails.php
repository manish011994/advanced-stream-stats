<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserSubscriptionDetails extends Model
{
    use HasFactory;

    protected $table = 'user_subscription_details';

    public static function saveUserSubscriptionDetails($userId, $planId, $subscribeDetails)
    {
        static::inactivePreviousSubscriptions($userId);
        $subscription = new UserSubscriptionDetails();
        $subscription->user_id = $userId;
        $subscription->plan_id = $planId;
        $subscription->subscription_id = $subscribeDetails->subscription->id;
        $subscription->plan_activated_on = ($subscribeDetails->subscription->billingPeriodStartDate)->format('Y-m-d');;
        $subscription->plan_valid_till = ($subscribeDetails->subscription->billingPeriodEndDate)->format('Y-m-d');;
        $subscription->amount_paid = $subscribeDetails->subscription->price;
        $subscription->status = 'active';
        $subscription->save();
        return $subscription;
    }

    public static function checkActiveSubscription($userId)
    {
        return UserSubscriptionDetails::where('user_id', $userId)
            ->where('status', 'active')
            ->first();
    }

    public static function inactivePreviousSubscriptions($userId)
    {
        return UserSubscriptionDetails::where('user_id', $userId)->update(['status' => 'inactive']);
    }
}
