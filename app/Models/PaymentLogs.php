<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PaymentLogs extends Model
{
    use HasFactory;

    protected $table = 'payment_initiate_logs';

    public static function savePaymentInitiateLog($type, $payload)
    {
        $paymentLogs = new PaymentLogs();
        $paymentLogs->user_id = Auth::user()->getAuthIdentifier();
        $paymentLogs->payload_type = $type;
        $paymentLogs->payload_data = json_encode($payload);
        return $paymentLogs->save();
    }
}
