<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubscriptionPlans extends Model
{
    use HasFactory;

    public static function getActivePlans()
    {
        return SubscriptionPlans::where('status', 'active')
            ->get();
    }
}
